# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [Unreleased]

### Added

- Possibility to delete baskets #5 !31 [@alex.simm](https://gitlab.com/alex.simm).

### Changed

- Loading all images via OkHttp and using caching #5 !31 [@alex.simm](https://gitlab.com/alex.simm).

## [0.0.2] - 2019-02-23

### Added

- nothing

## [0.0.1] - 2019-02-23

### Added

- everything
