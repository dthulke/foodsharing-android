package de.foodsharing.utils

import de.foodsharing.BuildConfig

/** The base URL of the API */
val BASE_URL = BuildConfig.BASE_URL

val LOG_TAG = "foodsharing"

val DEFAULT_USER_PICTURE = "$BASE_URL/img/130_q_avatar.png"

val PICTURE_FORMAT = "jpg"
val PICTURE_MIME_TYPE = "image/jpg"

val ONE_MEGABYTE = 1024 * 1024L
/** Size of the Http cache, set to 10 MB */
val CACHE_SIZE = 10 * ONE_MEGABYTE