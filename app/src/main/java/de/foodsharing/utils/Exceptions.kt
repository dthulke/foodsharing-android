package de.foodsharing.utils

import android.util.Log
import io.sentry.Sentry

fun captureException(t: Throwable) {
    Log.e(LOG_TAG, t.localizedMessage, t)
    Sentry.capture(t)
}