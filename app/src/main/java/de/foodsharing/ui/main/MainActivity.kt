package de.foodsharing.ui.main

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.TextView
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.drawer_layout
import kotlinx.android.synthetic.main.activity_main.main_pager
import kotlinx.android.synthetic.main.activity_main.main_tab_layout
import kotlinx.android.synthetic.main.activity_main.nav_view
import kotlinx.android.synthetic.main.activity_main.toolbar
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, Injectable {

    @Inject
    lateinit var presenter: MainContract.Presenter

    @Inject
    lateinit var auth: AuthService

    private var drawerToggle: ActionBarDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }

        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawerToggle?.syncState()

        // Show the current user name in the side bar
        val headerView = nav_view.getHeaderView(0)
        val accountName = headerView.findViewById<TextView>(R.id.account_name)
        accountName.text = auth.currentUser?.name

        val pagerAdapter = MainPagerAdapter(supportFragmentManager)

        main_pager.adapter = pagerAdapter
        main_tab_layout.setupWithViewPager(main_pager)

        val context = this
        val initiallySelectedTabIndex = 0
        val tabColor = R.color.colorPrimaryABitLighter
        val tabColorSelected = R.color.colorPrimaryLight

        main_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val textView = tab?.customView as TextView
                textView.setTextColor(ContextCompat.getColor(context, tabColor))
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        for (position in 0 until pagerAdapter.count) {
            main_tab_layout.getTabAt(position)?.apply {
                val textView = LayoutInflater.from(context).inflate(pagerAdapter.getPageTextView(position), null) as TextView
                if (position == initiallySelectedTabIndex) {
                    textView.setTextColor(ContextCompat.getColor(context, tabColorSelected))
                } else {
                    textView.setTextColor(ContextCompat.getColor(context, tabColor))
                }
                customView = textView
            }
        }

        main_tab_layout.getTabAt(initiallySelectedTabIndex)?.select()

        // set actions for menu items in the drawer
        nav_view.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            drawer_layout.closeDrawers()

            when (menuItem.itemId) {
                /*
                R.id.nav_account -> {
                    // TODO
                }
                */
                R.id.nav_logout -> logout()
            }

            true
        }

        presenter.attach(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        drawerToggle?.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Logs out the user and switches to the LoginActivity. This is called when the logout button
     * in the drawer is selected.
     */
    private fun logout() {
        presenter.logout()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }
}
