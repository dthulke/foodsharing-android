package de.foodsharing.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import de.foodsharing.R
import de.foodsharing.ui.baskets.BasketsFragment
import de.foodsharing.ui.conversations.ConversationsFragment
import de.foodsharing.ui.map.MapFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> ConversationsFragment()
        1 -> BasketsFragment()
        2 -> MapFragment()
        else -> throw Exception("invalid tab id!")
    }

    fun getPageTextView(position: Int): Int = when (position) {
        0 -> R.layout.tab_conversation
        1 -> R.layout.tab_basket
        2 -> R.layout.tab_map
        else -> throw Exception("invalid tab id!")
    }
}