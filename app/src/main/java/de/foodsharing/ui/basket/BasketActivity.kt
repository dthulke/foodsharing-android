package de.foodsharing.ui.basket

import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.services.AuthService
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.utils.CachedResourceLoader
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.activity_basket.*
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

class BasketActivity : BaseActivity(), BasketContract.View, Injectable {

    private val dateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault())

    @Inject
    lateinit var presenter: BasketContract.Presenter

    @Inject
    lateinit var resourceLoader: CachedResourceLoader

    @Inject
    lateinit var auth: AuthService

    private var basket: Basket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        setContentView(R.layout.activity_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.hasExtra("basket")) {
            basket = intent.getSerializableExtra("basket") as Basket
            basket?.let {
                supportActionBar?.title = "Basket: #${it.id}"
                display(it)
            }
        } else {
            val id = intent.getIntExtra("id", -1)
            supportActionBar?.title = "Basket: #$id"
            presenter.fetch(id)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        basket?.let {
            if (it.creator.id == auth.currentUser?.id) menuInflater.inflate(R.menu.basket_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.basket_remove_button -> {
            basket?.let {
                Utils.showQuestionDialog(this, getString(R.string.basket_remove_question)) { result ->
                    if (result) {
                        progress_bar.visibility = VISIBLE
                        presenter.removeBasket(it)
                    }
                }
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(basket: Basket) {
        this.basket = basket
        findViewById<TextView>(R.id.basket_created_at).text = dateFormat.format(basket.createdAt)
        findViewById<TextView>(R.id.basket_valid_until).text = dateFormat.format(basket.until)
        findViewById<TextView>(R.id.basket_description).text = basket.description
        findViewById<TextView>(R.id.basket_creator_name).text = basket.creator.name

        invalidateOptionsMenu()

        basket_content_view.visibility = VISIBLE
        progress_bar.visibility = GONE

        if (basket.picture.isNullOrEmpty()) findViewById<ImageView>(R.id.basket_picture).visibility = GONE
        else basket.picture?.let { presenter.loadPicture(it) }

        presenter.loadUserPicture(basket.creator)
    }

    override fun displayPicture(picture: Bitmap) {
        findViewById<ImageView>(R.id.basket_picture).setImageBitmap(picture)
    }

    override fun displayUserPicture(picture: Bitmap) {
        findViewById<ImageView>(R.id.basket_creator_picture).setImageBitmap(picture)
    }

    override fun basketRemoved() {
        finish()
    }
}
