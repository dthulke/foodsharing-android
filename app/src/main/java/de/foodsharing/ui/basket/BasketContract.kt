package de.foodsharing.ui.basket

import android.graphics.Bitmap
import de.foodsharing.model.Basket
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseContract

class BasketContract {

    interface View : BaseContract.View {
        fun display(basket: Basket)
        fun displayPicture(picture: Bitmap)
        fun displayUserPicture(picture: Bitmap)
        fun basketRemoved()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch(id: Int)
        fun loadPicture(url: String)
        fun loadUserPicture(user: User)
        fun removeBasket(basket: Basket)
    }
}