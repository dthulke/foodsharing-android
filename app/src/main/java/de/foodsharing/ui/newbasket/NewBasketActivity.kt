package de.foodsharing.ui.newbasket

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.FileProvider
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.utils.Utils
import de.foodsharing.utils.captureException
import kotlinx.android.synthetic.main.activity_new_basket.basket_add_picture_button
import kotlinx.android.synthetic.main.activity_new_basket.basket_checkbox_message
import kotlinx.android.synthetic.main.activity_new_basket.basket_checkbox_phone
import kotlinx.android.synthetic.main.activity_new_basket.basket_description_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_mobile_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_phone_input
import kotlinx.android.synthetic.main.activity_new_basket.basket_picture
import kotlinx.android.synthetic.main.activity_new_basket.basket_publish_button
import kotlinx.android.synthetic.main.activity_new_basket.basket_validity_spinner
import kotlinx.android.synthetic.main.activity_new_basket.basket_weight_spinner
import kotlinx.android.synthetic.main.activity_new_basket.toolbar
import java.io.File
import java.io.IOException
import javax.inject.Inject

class NewBasketActivity : BaseActivity(), NewBasketContract.View, Injectable {

    @Inject
    lateinit var presenter: NewBasketContract.Presenter

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_SHOW_IMAGE = 2

    private var file: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        rootLayoutID = R.id.new_basket_root
        setContentView(R.layout.activity_new_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_new_title)

        basket_checkbox_phone.setOnCheckedChangeListener { _, checked ->
            basket_phone_input.visibility = if (checked) VISIBLE else GONE
            basket_mobile_input.visibility = if (checked) VISIBLE else GONE
        }
        basket_publish_button.setOnClickListener { publishBasket() }
        basket_add_picture_button.setOnClickListener { capturePhoto() }
        basket_picture.setOnClickListener {
            if (file != null) {
                startActivityForResult(
                        Intent(this, PictureActivity::class.java).apply {
                            putExtra("pictureFile", file)
                        }, REQUEST_SHOW_IMAGE
                )
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(basket: Basket) {
        // show details of created basket in an activity
        val intent = Intent(this, BasketActivity::class.java).also { intent ->
            intent.putExtra("basket", basket)
        }
        TaskStackBuilder.create(this)
                .addNextIntentWithParentStack(intent)
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                ?.let {
                    it.send()
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                }
    }

    override fun showError(message: String) {
        showMessage(message)
        basket_publish_button.isEnabled = true
    }

    /**
     * Starts the camera activity to take a photo.
     */
    private fun capturePhoto() {
        try {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
                intent.resolveActivity(packageManager)?.also {
                    file = Utils.createImageFile(this).also {
                        val photoURI: Uri =
                                FileProvider.getUriForFile(
                                        this, "${BuildConfig.APPLICATION_ID}.fileprovider", it
                                )
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    }

                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
                }
            }
        } catch (e: IOException) {
            captureException(e)
        }
    }

    /**
     * Checks the input and publishes the basket.
     */
    private fun publishBasket() {
        basket_publish_button.isEnabled = false

        // collect all input values
        val description = basket_description_input.text.toString().trim()
        val weightIdx = basket_weight_spinner.selectedItemPosition
        val weight = resources.getStringArray(R.array.basket_weight_values)[weightIdx].toFloat()
        val contactByMessage = basket_checkbox_message.isChecked
        val contactByPhone = basket_checkbox_phone.isChecked
        val phone =
                if (basket_checkbox_phone.isChecked) basket_phone_input.text.toString().trim() else null
        val mobile =
                if (basket_checkbox_phone.isChecked) basket_mobile_input.text.toString().trim() else null
        val lifetimeIdx = basket_validity_spinner.selectedItemPosition
        val lifetime =
                resources.getStringArray(R.array.basket_validity_values)[lifetimeIdx].toInt()

        // simple client-side validation
        var error: String? = null
        if (description.isEmpty()) {
            error = getString(R.string.basket_error_description)
        } else if (!contactByMessage && !contactByPhone) {
            error = getString(R.string.basket_error_contacttype)
        } else if (contactByPhone && (phone.isNullOrEmpty() && mobile.isNullOrEmpty())) {
            error = getString(R.string.basket_error_phone)
        }

        if (error != null) {
            showMessage(error, Snackbar.LENGTH_LONG)
            basket_publish_button.isEnabled = true
        } else {
            presenter.publish(
                    description,
                    phone,
                    mobile,
                    contactByMessage,
                    weight,
                    lifetime,
                    null,
                    file
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SHOW_IMAGE -> {
                if (resultCode == Activity.RESULT_CANCELED) {
                    // remove picture from view
                    file = null
                    basket_picture.setImageBitmap(
                            BitmapFactory.decodeResource(
                                    resources,
                                    R.drawable.basket_default_picture
                            )
                    )
                }
            }
            REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    // show picture in view
                    file?.let {
                        Utils.loadRescaledBitmap(
                                it.absolutePath,
                                basket_picture.width,
                                basket_picture.height
                        )
                                .let {
                                    basket_picture.setImageBitmap(it)
                                }
                    }
                }
            }
        }
    }
}
