package de.foodsharing.ui.newbasket

import de.foodsharing.api.BasketAPI
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.PICTURE_MIME_TYPE
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

class NewBasketPresenter @Inject constructor(private val baskets: BasketAPI) :
        BasePresenter<NewBasketContract.View>(), NewBasketContract.Presenter {

    private val contactTypeMessage = 1
    private val contactTypePhone = 2

    override fun publish(
        description: String,
        phone: String?,
        mobile: String?,
        contactByMessage: Boolean,
        weight: Float,
        lifetime: Int,
        location: Pair<Double, Double>?,
        picture: File?
    ) {
        val contactTypes = ArrayList<Int>()

        if (phone != null && mobile != null) contactTypes.add(contactTypePhone)
        if (contactByMessage) contactTypes.add(contactTypeMessage)

        request(
                baskets.create(
                        description,
                        contactTypes.toTypedArray(),
                        phone,
                        mobile,
                        weight,
                        lifetime,
                        location?.first,
                        location?.second
                ), {
            val basket = it.basket!!

            if (picture != null) {
                val body = RequestBody.create(MediaType.get(PICTURE_MIME_TYPE), picture)
                request(baskets.setPicture(basket.id, body), {
                    view?.display(it.basket!!)
                }, {
                    view?.showError(it.localizedMessage)
                    view?.display(basket)
                })
            } else view?.display(basket)
        }, {
            view?.showError(it.localizedMessage)
        })
    }
}