package de.foodsharing.ui.conversations

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.R
import java.util.Date

/**
 * Formats the date that is displayed above chat messages.
 */
class ConversationDateFormatter(val context: Context) : DateFormatter.Formatter {

    override fun format(date: Date): String {
        return when {
            DateFormatter.isToday(date) -> DateFormatter.format(date, DateFormatter.Template.TIME)
            DateFormatter.isYesterday(date) -> context.getString(R.string.date_yesterday)
            DateFormatter.isCurrentYear(date) -> DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH)
            else -> DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)
        }
    }
}