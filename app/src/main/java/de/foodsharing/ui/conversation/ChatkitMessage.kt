package de.foodsharing.ui.conversation

import android.text.Html
import com.stfalcon.chatkit.commons.models.IMessage
import de.foodsharing.model.Message
import de.foodsharing.model.User
import java.util.Date

class ChatkitMessage(val message: Message, author: User) : IMessage {

    private val user = ChatkitUser(author)

    override fun getId() = message.id.toString()

    @Suppress("DEPRECATION")
    override fun getText() = Html.fromHtml(message.body).toString()

    override fun getUser() = user

    override fun getCreatedAt(): Date = message.time
}