package de.foodsharing.ui.conversation

import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.Message
import de.foodsharing.ui.base.BaseContract

class ConversationContract {

    interface View : BaseContract.View {
        fun display(conversation: ConversationDetail)
        fun addMessage(message: Message)
        fun addHistory(messages: List<Message>)
        fun clearInput()
        fun enableButton()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch(id: Int)
        fun fetchHistory(id: Int, number: Int, offset: Int)
        fun sendMessage(cid: Int, body: String)
    }
}