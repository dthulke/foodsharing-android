package de.foodsharing.di

import dagger.Module
import dagger.Provides
import de.foodsharing.ui.basket.BasketContract
import de.foodsharing.ui.basket.BasketPresenter
import de.foodsharing.ui.baskets.BasketsContract
import de.foodsharing.ui.baskets.BasketsPresenter
import de.foodsharing.ui.conversation.ConversationContract
import de.foodsharing.ui.conversation.ConversationPresenter
import de.foodsharing.ui.conversations.ConversationsContract
import de.foodsharing.ui.conversations.ConversationsPresenter
import de.foodsharing.ui.login.LoginContract
import de.foodsharing.ui.login.LoginPresenter
import de.foodsharing.ui.main.MainContract
import de.foodsharing.ui.main.MainPresenter
import de.foodsharing.ui.map.MapContract
import de.foodsharing.ui.map.MapPresenter
import de.foodsharing.ui.newbasket.NewBasketContract
import de.foodsharing.ui.newbasket.NewBasketPresenter

@Module
class PresentersModule {

    @Provides
    fun provideLoginPresenter(presenter: LoginPresenter): LoginContract.Presenter = presenter

    @Provides
    fun provideConversationsPresenter(presenter: ConversationsPresenter): ConversationsContract.Presenter = presenter

    @Provides
    fun provideConversationPresenter(presenter: ConversationPresenter): ConversationContract.Presenter = presenter

    @Provides
    fun provideMainPresenter(presenter: MainPresenter): MainContract.Presenter = presenter

    @Provides
    fun provideMapPresenter(presenter: MapPresenter): MapContract.Presenter = presenter

    @Provides
    fun provideBasketsPresenter(presenter: BasketsPresenter): BasketsContract.Presenter = presenter

    @Provides
    fun provideBasketPresenter(presenter: BasketPresenter): BasketContract.Presenter = presenter

    @Provides
    fun provideNewBasketPresenter(presenter: NewBasketPresenter): NewBasketContract.Presenter = presenter
}